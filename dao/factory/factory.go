package factory

import (
	"log"

	"github.com/alfredomendozacap/DAOPattern/dao/interfaces"
	"github.com/alfredomendozacap/DAOPattern/dao/mysql"
	"github.com/alfredomendozacap/DAOPattern/dao/psql"
)

// DAO devuelve la implementación especifica
func DAO(e string) interfaces.UserDAO {
	var i interfaces.UserDAO
	switch e {
	case "postgres":
		i = psql.UserImplPsql{}
	case "mysql":
		i = mysql.UserImplMySQL{}
	default:
		log.Fatalf("El motor %s no esta implementado", e)
		return nil
	}
	return i
}
