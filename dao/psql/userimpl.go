package psql

import (
	"github.com/alfredomendozacap/DAOPattern/models"
)

// UserImplPsql estructura vacia para usuario en Postgres
type UserImplPsql struct{}

// CreateUser función para crear usuario en Postgres
func (dao UserImplPsql) CreateUser(u *models.User) error {
	query := `INSERT INTO users (firstname, lastname, email) VALUES ($1, $2, $3) RETURNING id`
	db := get()
	defer db.Close()

	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	row := stmt.QueryRow(u.FirstName, u.LastName, u.Email)
	row.Scan(&u.ID)
	return nil
}

// GetAllUsers devuelve todos los usuarios de la BD en Postgres
func (dao UserImplPsql) GetAllUsers() ([]models.User, error) {
	users := make([]models.User, 0)
	query := `SELECT id, firstname, lastname, email FROM users`

	db := get()
	defer db.Close()

	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var row models.User
		err = rows.Scan(&row.ID, &row.FirstName, &row.LastName, &row.Email)
		if err != nil {
			return nil, err
		}
		users = append(users, row)
	}
	return users, nil
}
