package psql

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/alfredomendozacap/DAOPattern/utilities"
	// Package pq is a pure Go Postgres driver for the database/sql package
	_ "github.com/lib/pq"
)

func get() *sql.DB {
	config, err := utilities.GetConfiguration()
	if err != nil {
		log.Fatal(err)
	}

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", config.User, config.Password, config.Server, config.Port, config.Database)

	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatal(err)
	}
	return db
}
