package interfaces

import (
	"github.com/alfredomendozacap/DAOPattern/models"
)

// UserDAO interface para implementar en los motores de BD
type UserDAO interface {
	CreateUser(u *models.User) error
	/* UpdateUser(u *models.User) error
	DeleteUser(i int) error
	GetUserByID(i int) (models.User, error) */
	GetAllUsers() ([]models.User, error)
}
