package mysql

import (
	"github.com/alfredomendozacap/DAOPattern/models"
)

// UserImplMySQL estructura vacia para usuario en MySql
type UserImplMySQL struct{}

// CreateUser función para crear usuario en MySql
func (dao UserImplMySQL) CreateUser(u *models.User) error {
	query := `INSERT INTO users (first_name, last_name, email) VALUES (?, ?, ?)`
	db := get()
	defer db.Close()

	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	result, err := stmt.Exec(u.FirstName, u.LastName, u.Email)
	if err != nil {
		return err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return err
	}

	u.ID = int(id)
	return nil
}

// GetAllUsers devuelve todos los usuarios de la BD en MySql
func (dao UserImplMySQL) GetAllUsers() ([]models.User, error) {
	users := make([]models.User, 0)
	query := `SELECT id, first_name, last_name, email FROM users`

	db := get()
	defer db.Close()

	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var row models.User

		err := rows.Scan(&row.ID, &row.FirstName, &row.LastName, &row.Email)
		if err != nil {
			return nil, err
		}

		users = append(users, row)
	}
	return users, nil
}
