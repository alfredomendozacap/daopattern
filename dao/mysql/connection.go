package mysql

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/alfredomendozacap/DAOPattern/utilities"
	// Package mysql is a pure Go MySQL driver for the database/sql package
	_ "github.com/go-sql-driver/mysql"
)

func get() *sql.DB {
	config, err := utilities.GetConfiguration()
	if err != nil {
		log.Fatal(err)
	}

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?tls=false&autocommit=true", config.User, config.Password, config.Server, config.Port, config.Database)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	return db
}
