package models

// Configuration estructura de configuración
type Configuration struct {
	Engine   string
	Server   string
	Port     string
	User     string
	Password string
	Database string
}
