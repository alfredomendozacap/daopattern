package models

// User modelo de users
type User struct {
	ID        int
	FirstName string
	LastName  string
	Email     string
}
