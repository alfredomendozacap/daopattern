package main

import (
	"fmt"
	"log"

	"github.com/alfredomendozacap/DAOPattern/dao/factory"
	"github.com/alfredomendozacap/DAOPattern/models"
	"github.com/alfredomendozacap/DAOPattern/utilities"
)

func main() {
	config, err := utilities.GetConfiguration()
	if err != nil {
		log.Fatal(err)
	}

	userDAO := factory.DAO(config.Engine)
	user := models.User{}

	fmt.Print("Nombre: ")
	fmt.Scan(&user.FirstName)
	fmt.Print("Apellido: ")
	fmt.Scan(&user.LastName)
	fmt.Print("Correo Electrónico: ")
	fmt.Scan(&user.Email)

	/* err = userDAO.CreateUser(&user)
		if err != nil {
			log.Fatal(err)
		}

	        fmt.Println(user) */
	users, err := userDAO.GetAllUsers()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(users)
}
