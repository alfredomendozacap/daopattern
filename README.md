# DAO design pattern in Go

You won't need to create two main functions to use a DB in _MySQL_ and another in _Postgres_
___
If you want to implement another DB of another DB engine, **just do it!!!**